package cs449.collin.com.imagetts;

import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.os.Environment;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import java.io.File;
import java.io.FileFilter;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Collin on 10/22/2018.
 */

public class ChatHeadService extends Service {

    Bitmap bitmap;
    private WindowManager windowManager;
    private static ImageView chatHead;
    WindowManager.LayoutParams params;
    TextToSpeech tts;
    TextView theText;
    private static final int MAX_CLICK_DURATION = 200; //To differentiate between "clicks" on the chathead
    private long startClickTime;

    @Override public IBinder onBind(Intent intent) {
        // Not used
        return null;
    }

    @Override public void onCreate() {
        super.onCreate();

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        chatHead = new ImageView(this);
        chatHead.setImageResource(R.drawable.chathead);



        params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.x = 0;
        params.y = 100;

        windowManager.addView(chatHead, params);
        chatHead.setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = params.x;
                        initialY = params.y;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        startClickTime = Calendar.getInstance().getTimeInMillis();







                        return true;
                    case MotionEvent.ACTION_UP:

                        String photoPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM) + "/Camera/IMG_20180219_041420.jpg";

                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                        bitmap = BitmapFactory.decodeFile(photoPath, options);

                        //Image-To-Speech if click
                        long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                        if(clickDuration < MAX_CLICK_DURATION) { //Then click event has occurred
                            tts=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {

                                @Override
                                public void onInit(int status) {
                                    // TODO Auto-generated method stub
                                    if(status == TextToSpeech.SUCCESS){
                                        int result=tts.setLanguage(Locale.US);
                                        if(result==TextToSpeech.LANG_MISSING_DATA ||
                                                result==TextToSpeech.LANG_NOT_SUPPORTED){
                                            Log.e("error", "This Language is not supported");
                                        }
                                        else{
                                            TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();
                                            if(!textRecognizer.isOperational()){
                                                Toast.makeText(getApplicationContext(),"Unable to obtain text from image", Toast.LENGTH_SHORT).show();
                                            } else{


                                                assert(bitmap != null);
                                                    Frame frame = new Frame.Builder().setBitmap(bitmap).build();

                                                    SparseArray<TextBlock> items = textRecognizer.detect(frame);
                                                    StringBuilder stringBuilder = new StringBuilder();

                                                    for (int i = 0; i < items.size(); i++) {
                                                        TextBlock myTextBlock = items.valueAt(i);
                                                        stringBuilder.append(myTextBlock.getValue());
                                                        stringBuilder.append("\n");
                                                    }
                                                    String foundText = stringBuilder.toString();
                                                    //Translate


                                                    theText.setText(foundText);
                                                    tts.speak(foundText, TextToSpeech.QUEUE_FLUSH, null);



                                            }
                                        }
                                    }
                                    else
                                        Log.e("error", "Initilization Failed!");
                                }
                            });
                        }
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        params.x = initialX + (int) (event.getRawX() - initialTouchX);
                        params.y = initialY + (int) (event.getRawY() - initialTouchY);
                        windowManager.updateViewLayout(chatHead, params);
                        return true;
                }
                return false;
            }
        });


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (chatHead != null) windowManager.removeView(chatHead);
        if (tts != null){
            tts.stop();
            tts.shutdown();
        }
    }

    public static ImageView getChatHead(){
        return chatHead;
    }

    public static File lastFileModified(String dir) {
        File fl = new File(dir);
        File[] files = fl.listFiles(new FileFilter() {
            public boolean accept(File file) {
                return file.isFile();
            }
        });
        long lastMod = Long.MIN_VALUE;
        File choice = null;
        for (File file : files) {
            if (file.lastModified() > lastMod) {
                choice = file;
                lastMod = file.lastModified();
            }
        }
        return choice;
    }

}
