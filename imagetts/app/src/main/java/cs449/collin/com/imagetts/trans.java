package cs449.collin.com.imagetts;

/**
 * Created by Collin on 11/19/2018.
 */

import com.google.cloud.translate.Translate;
import com.google.cloud.translate.Translate.TranslateOption;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;

public class trans {
    public static String translate(String theText){
        Translate translate = TranslateOptions.getDefaultInstance().getService();

        // The text to translate

        // Translates some text into Russian
        Translation translation =
                translate.translate(
                        theText,
                        TranslateOption.sourceLanguage("en"),
                        TranslateOption.targetLanguage("ru"));

        return translation.getTranslatedText();

    }
    public static void main(String[] args){
        System.out.println(translate("help"));
    }
}
