# CS449

To run - simply launch on android studio.  Press add bubble, exit application.
You may move the red square button around.  If you click on the red square button for less than 200ms it will register as a click
If clicked, it will pull data from device storage, load it into a bitmap, read text from the image, and output it as speech.
If your device saves images in a different location or there are no images in the directory, the bitmap will be null and the program will throw an exception.  

Instead of searching for the most recent screen shot, I changed it last second so you could test this yourselves(your device would probably have saved it in a 
different location from what I have researched).  I left the algorithm for finding the most recent file in a directory as a function as proof that it was done.

Just rename the file and place it into the directory that is specified respectively in the "chathead" service class and follow the steps above.